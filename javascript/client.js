function Comm(port, msgRecievedCallback){
    this.localhostUrl = "ws://localhost:" + port;
    this.msgRecievedCallback = msgRecievedCallback;
    console.log("comm created");
}

Comm.prototype.constructor = Comm;

Comm.prototype.start = function (){
    this.websocket = new WebSocket(this.localhostUrl);
    this.websocket.onopen = function(){
        console.log("comm connected");
    }.bind(this);

    this.websocket.onmessage = function (event){
        var msgObject = JSON.parse(event.data);
        console.log(msgObject);
        this.msgRecievedCallback(msgObject);
    }.bind(this);
    this.websocket.onclose = function(evt){
        if(evt.code !==3001){
            console.log("ws connection error");
            this.msgRecievedCallback(
                     {
                         name:"websocket",
                         type:"error",
                         data:{
                             msg:"connection error. UI might be opened in another window, or server might shut down."
                         }
                     });
        }
    }.bind(this);
}

Comm.prototype.msg = function(msg){
    this.msgRecievedCallback(msg);
}

Comm.prototype.sendMessage = function(message){
    console.log(message);
    this.websocket.send(JSON.stringify(message));
}

Comm.prototype.sendStartGameMsg = function(size, winlength, starting){
    this.sendMessage(
             {
                 name:"game",
                 type:"start",
                 data: {
                     size:size,
                     winlength:winlength,
                     starting:starting
                 }
             });
}

Comm.prototype.sendHumanPlayed = function(x,y){
    this.sendMessage(
             {
                 name:"human",
                 type:"coords",
                 data: {
                     x:x,
                     y:y
                 }
             });
}

Comm.prototype.sendEndRound = function(){
    this.sendMessage(
             {
                 name:"game",
                 type:"endRound"
             });
}

Comm.prototype.sendEndGame = function (){
    this.sendMessage(
             {
                 name:"game",
                 type:"end"
             });
}
