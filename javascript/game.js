function Game (){
    this.comm = new Comm(9000, function(msg){
        if(msg.type === "error"){
            console.log(msg);
            this.exceptionRecieved(msg);
            return;
        }
        var data = msg.data;
        switch(msg.name){
        case "board":
            console.log('board msg:');
            switch(msg.type){
            case "end":
                this.gameEnd(data);
                break;
            default:
                console.log("unmapped message");
                return;
            }
            break;
        case "computer":
            console.log('computer msg:');
            this.board.recieveCoords(msg);
            break;
        case "game":
            this.gameEnd(data);
            break;
        default:
            console.log('message with unmapped name');
            return;
        }
    }.bind(this));

    this.board = new Board(this.comm);
    this.winLengthInput = $('#winLengthInput');
    this.sizeInput = $('#sizeInput');
    this.startingPlayer = $('#startingPlayer');
    this.resultDiv = $('#result');
    this.showMenu = function (){
        $('#gameStart').modal({
                          backdrop: 'static',
                          keyboard: false
                        });
    }
};

Game.prototype.constructor = Game;

Game.prototype.init = function(){
    $('#gameStartBtn').click(function(){
        if(this.checkInputContents()){
            var winLength = this.winLengthInput.val();
            var size = this.sizeInput.val();
            var startingPlayer = this.startingPlayer.val();
            this.board.generateBoard(size);
            this.comm.sendStartGameMsg(size, winLength, startingPlayer);
            $('#gameStart').modal('hide');
        }
    }.bind(this));
    this.showMenu();
};

Game.prototype.start = function (){
    console.log("starting game");
    this.comm.start();
};

Game.prototype.gameStatus = function(msg){
    if (msg.name !== "game"){
        console.log('wrong msg in game status:');
        console.log(msg);
        return;
    }
};

Game.prototype.checkInputContents = function(){
    var winLength = parseInt(this.winLengthInput.val(),10);
    var size = parseInt(this.sizeInput.val(),10);
    var resultDiv = $('#result');
    if (isNaN(winLength) || isNaN(size) ||
            (winLength < this.winLengthInput.attr('min') || winLength > this.winLengthInput.attr('max')) ||
            (size < this.sizeInput.attr('min') || size > this.sizeInput.attr('max'))){
        resultDiv.empty();
        resultDiv.html("Check number boxes, you might have inputted incorrect values.");
        return false;
    }

    if(winLength > size){
        resultDiv.empty();
        resultDiv.html("Lenght of the winning row must not be longer than size of board.");
        return false;
    }
    return true;
};

Game.prototype.gameEnd = function(data){
    console.log("ending game");
    var resultDiv = $('#result');
    resultDiv.empty();
    var msg = "unknown res";
    switch(data.winner){
    case "human":
        msg = "You won!";
        break;
    case "computer":
        msg = "You lost!";
        break;
    case "draw":
        msg = "It was a draw!";
        break;
    }
    resultDiv.html(msg);
    this.showMenu();
};

Game.prototype.exceptionRecieved = function(msg){
    console.log("exception recieved");
    var resultDiv = $('#result');
    resultDiv.empty();
    resultDiv.html("Error recieved from "
                   + msg.name + ": " + msg.data.msg);
    this.showMenu();
};

Game.prototype.end = function (){
    this.comm.sendEndGame();
}

var game = new Game();
game.init();
game.start();

window.onbeforeunload = function () {
    game.end();
 }
