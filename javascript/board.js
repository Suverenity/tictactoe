function Board(comm){
    console.log("board");
    this.comm = comm;
    this.size = 10;
    this.tiles = [];
    this.isClickable = true;
    this.colorMap = [];
    this.colorMap['human'] = 'blue';
    this.colorMap['computer'] = 'red';
}

Board.prototype.constructor = Board;

Board.prototype.generateBoard = function (size){
    if(size){
        this.size = size;
    }
    var tableDiv = $("#tablebase");
    tableDiv.empty();
    this.tiles = [];
    for(i=0; i < this.size; ++i){
        var htmlRow = this.createRow();
        var row = [];
        for(j=0; j < this.size; j++){
            var tile = new Tile(j,i, this);
            row.push(tile);
            htmlRow.append(tile.getHtml());
        }
        this.tiles.push(row);
        tableDiv.append(htmlRow);
    }
    this.isClickable = true;
    console.log(this.tiles);
}

Board.prototype.createRow = function(){
    return $('<tr/>');
}

Board.prototype.clicked = function(){
    this.isClickable = false;
}

Board.prototype.sendCoords = function (x,y){
    this.comm.sendHumanPlayed(x,y);
}

Board.prototype.recieveCoords = function(message){
    if(!message.name){
        console.log(message);
        console.log("got weird message: ");
    }
    var tile = this.tiles[message.data.y][message.data.x];
    if(!tile){
        console.log('no tile at coords: x-' + message.data.x + ', y-' + message.data.y);
    }
    var color = this.colorMap[message.name];
    if(color){
        tile.changeBackground(color);
        this.isClickable = true;
    }
}

Board.prototype.boardStatus = function(data){

}

/////////////////////////////////

function Tile(x, y, board){
    this.x = x;
    this.y = y;
    this.wasClicked = false;
    this.td = $('<td/>')//.html('x: ' +x + '<br>' + 'y: ' + y);
    this.board = board;
    this.td.click(function(){
//        if(this.wasClicked ){
            this.clicked();
//            this.board.clicked();
//        }
    }.bind(this));
}

Tile.prototype.constructor = Tile;

Tile.prototype.clicked = function (){
    if(!this.wasClicked && this.board.isClickable){
        this.board.sendCoords(this.x, this.y);
        this.changeBackground(this.board.colorMap['human']);
        this.board.clicked();
        this.wasClicked = true;
    }
}

Tile.prototype.getHtml = function (){
    return this.td;
}

Tile.prototype.changeBackground = function (color){
    if(!this.wasClicked){
        this.td.css("background-color", color);
        this.wasClicked = true;
    }
}
