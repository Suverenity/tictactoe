#include "Player.h"
#include "MessageFactory.h"

#include <boost/thread/mutex.hpp>

Player::Player(std::string name, Tile tile, MessageFactory &fact)
	: TTTObject(name), MessageSender(fact), m_tileType(tile)
{}

void Player::endRound()
{
	boost::mutex mutex;
	mutex.lock();
	m_endRequested = true;
	mutex.unlock();
}

void Player::moveAccepted()
{
	sendMessage(m_fact.createCoordsMessage(
					this->getName(),
					m_nextMove.x, m_nextMove.y));
}
