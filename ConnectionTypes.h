#ifndef CONNECTIONTYPES_H
#define CONNECTIONTYPES_H

#include "disable_warnings.h"
#include <boost/signals2/signal.hpp>
#include "enable_warnings.h"

#include "Message.h"

/**
 * @brief typedef MessageSignal used in MessageSender class
 *
 * Signal class used to communicate between MessageSender children and Communicator
 */

typedef boost::signals2::signal<void (Message)> MessageSignal;

/**
 * @brief typedef MessageSlot used in MessageReciever class
 *
 * Function used as slot to MessageSignal.
 */
typedef boost::function<void (Message)> MessageSlot ;

#endif // CONNECTIONTYPES_H
