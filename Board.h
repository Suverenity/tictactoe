#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <deque>

#include "TTTObject.h"
#include "Tile.h"

#define DEFAULT_SIZE 3

/**
 * @brief Basic representation of the TicTacToe board
 *
 * Board is simple NxN array of Tiles (see Tile.h) with methods detecting
 * board state (see Tile.h and Result enum) and possible winner.
 */

class Board
		: public TTTObject
{
public:
	using WinRecord = std::deque<Coords>;

	Board(size_t size);
	Board();
	Board(Board &other);

	size_t getEdgeLength() const;
	size_t getWinLength() const;
	void setEdgeLength(size_t value);
	void setWinLength(size_t value);
	bool fillTile(size_t row, size_t column, Tile value);
	Result getBoardState();
	void resetBoard();
	std::string getWinner() const;

	bool isTileEmpty(Coords coords);
	bool isEmpty();
	friend std::ostream& operator<< (std::ostream& os, const Board& board);
	Tile& operator() (size_t row, size_t column);
	Tile operator() (size_t row, size_t column) const;

	friend void swap(Board &first, Board& second);
	Board& operator =(Board other);

private:
	bool checkDraw();
	bool checkWin(size_t row, size_t column, Tile value);
	bool checkWinInRow(int xDir, int yDir,	size_t rootRow,
						size_t rootColumn, Tile value);
	bool checkWinInHalf(Coords root, Coords other, Tile value,
						bool &failedAdding,	WinRecord &winRecord,
						bool oppositeDir);
	bool checkTwoTilesToValue(size_t rowFirst, size_t columnFirst,
						size_t rowSecond, size_t columnSecond, Tile value);

	size_t m_size = DEFAULT_SIZE;
	size_t m_freeTiles = (DEFAULT_SIZE*DEFAULT_SIZE);
	Result m_boardState = Result::undecided;
	TileArray m_board;
	size_t m_winLength = DEFAULT_SIZE;
	std::string m_winner;

};

#endif // BOARD_H
