#ifndef LOGGER_H
#define LOGGER_H

#include <string>

/**
 * @brief The Logger class
 */

class Logger
{
public:
	Logger() = default;

	static void log(std::string message);
};

#endif // LOGGER_H
