#include "MessageSender.h"
#include "Message.h"

MessageSender::MessageSender(MessageFactory &fact)
	:m_fact(fact)
{ }

void MessageSender::connectSlotToSignal(MessageSlot slot)
{
	m_sendMessageSignal.connect(slot);
}

void MessageSender::sendMessage(Message msg)
{
	m_sendMessageSignal(msg.toString());
}
