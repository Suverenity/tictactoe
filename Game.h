#ifndef GAME_H
#define GAME_H

#include "MessageReciever.h"
#include "MessageSender.h"

#include "Communicator.h"
#include "Engine.h"

/**
 * @brief The Game class
 * Game class that serves as singleton owner to other objects:
 * communication classes (Communicator, MessageFactory) and game
 * classes (Engine).
 */

class Game
		: public TTTObject
		, public MessageSender
		, public MessageReciever
{
	struct NextRound{
		NextRound() = default;
		NextRound(std::string player, unsigned int size, unsigned int length)
			: startingPlayer(player), boardSize(size), winningLength(length)
		{}
		std::string startingPlayer = "";
		unsigned int boardSize = 0;
		unsigned int winningLength = 0;

		void reset(){
			startingPlayer = "";
			boardSize = 0;
			winningLength = 0;
		}
	};
public:
	Game();
	~Game();

	/**
	 * @brief Starts the game.
	 *
	 * Blocks executing thread until game is ended.
	 */
	void run();

	/**
	 * @brief Initializes game object
	 *
	 * Basically calls CommConnector class that connects all objects meant to communicate via JSON messages
	 */
	void initialize();

	/**
	 * @brief Ends game
	 */
	void end();

private:

	MessageFactory m_messageFactory;
	Communicator m_comm;
	Engine m_engine;
	bool m_keepRunning = true;
	NextRound m_nextRoundSettings;
	bool m_roundInProgress = false;

	void openBrowser();
	void startRound();
	void endRound();
	void logException(std::string exMsg);

	// MessageReciever interface
protected:
	void recieveMessage(Message msg);
};

#endif // GAME_H
