#ifndef ITTTTObject_H
#define ITTTTObject_H

#include <string>

/**
 * @brief The ITTTObject abstract base class
 *
 * This class is generaly used as parent
 * for classes that communicate with UI.
 */

class ITTTObject
{
public:
	virtual ~ITTTObject() = default;

	virtual std::string getName() const = 0;
};

#endif // ITTTTObject_H
