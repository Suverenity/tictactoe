#include "Computer.h"
#include "Board.h"
#include "Logger.h"

#include "boost/multi_array.hpp"

#include <random>
#include <climits>
#include <algorithm>


Computer::Computer(MessageFactory &fact)
	: Player("computer", Tile::computer, fact)
{}

void Computer::figureNextMove(Board board)
{
	if(!m_played || board.getEdgeLength() > 4){
		auto range = board.getEdgeLength();
		m_hasNextMove = false;
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> dis(0, range-1);
		m_nextMove.x = dis(gen);
		m_nextMove.y = dis(gen);
		m_played = true;
	}else {
		m_nextMove = minmaxStart(board);
	}
	m_hasNextMove = true;
}

Computer::MinMaxRes Computer::minmax(Board board, Tile val, unsigned long &succesfullDepth,
									 unsigned long depth, Coords coords)
{
	if(m_endRequested){
		// does not matter what is returned, just that function eds here
		return MinMaxRes::Draw;
	}
	if(depth > succesfullDepth){
		return val == Tile::computer ? MinMaxRes::HumanWon : MinMaxRes::ComputerWon;
	}
	if(!board.fillTile(coords.y,coords.x, val)){
		std::cout << board << std::endl;
		throw std::string("could not fill board: " + coords.toString());
	}
	switch (board.getBoardState()) {
	case Result::victory:
		if(val == Tile::computer){
			succesfullDepth = depth;
		}
		return val == Tile::computer ? MinMaxRes::ComputerWon : MinMaxRes::HumanWon;
	case Result::draw:
		return MinMaxRes::Draw;
	case Result::undecided:
		{
			MinMaxRecordVector record;
			for(size_t y = 0; y < board.getEdgeLength(); ++y){
				for(size_t x = 0; x < board.getEdgeLength(); ++x){
					if(board.isTileEmpty({x,y})){
						record.push_back({ minmax(board, val == Tile::computer ? Tile::human : Tile::computer,
										   succesfullDepth, depth+1, {x,y}), {x,y}});
					}
				}
			}
			switch (val) {
			case Tile::human:
				return std::max_element(record.begin(), record.end(), cmpFunction)->first;
			case Tile::computer:
				return std::min_element(record.begin(), record.end(), cmpFunction)->first;
			default:
				throw std::string ("default when not expected: " + coords.toString());
			}
			break;
		}
	}
	throw std::string ("end when should be done");
}

Coords Computer::minmaxStart(Board board)
{
	ulong succesfullDepth = ULONG_MAX, depth = 0;
	MinMaxRecordVector record;
	for(size_t y = 0; y < board.getEdgeLength(); ++y){
		for(size_t x = 0; x < board.getEdgeLength(); ++x){
			if(board.isTileEmpty({x,y})){
				record.push_back({minmax(board,  Tile::computer, succesfullDepth, depth+1, {x,y}),{x,y}});

			}
		}
	}
	return std::max_element(record.begin(), record.end(), cmpFunction)->second;
}

