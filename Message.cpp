#include "Message.h"

#include <sstream>

#include "disable_warnings.h"
#include <Poco/Dynamic/Var.h>
#include <Poco/JSON/Parser.h>
#include "enable_warnings.h"

Message::Message(std::string name, std::string msgType)
	:m_data(new Json::Object)
{
	m_data->set("name", name);
	m_data->set("type", msgType);
}

Message::Message(std::string msgJson)
{
	Json::Parser parser;
	m_data = parser.parse(msgJson).extract<Json::Object::Ptr>();
}

void Message::setName(std::string name)
{
	m_data->set("name", Poco::Dynamic::Var(name.c_str()));
}

void Message::setData(Json::Object data)
{
	m_data->set("data", data);
}

std::string Message::getName()
{
	auto res = m_data->get("name");
	if (res.isEmpty()){
		throw std::invalid_argument("no name value specified");
	}
	return res.toString();
}

std::string Message::toString()
{
	std::ostringstream ostr;
	Json::Stringifier::stringify(m_data, true, ostr);
	return ostr.str();
}

Poco::JSON::Object::Ptr Message::getData()
{
	return m_data->get("data").extract<Poco::JSON::Object::Ptr>();
}

std::string Message::getType()
{
	auto res = m_data->get("type");
	if (res.isEmpty()){
		throw std::invalid_argument("no type value specified");
	}
	return res.toString();}

void Message::setType(std::string type)
{
	m_data->set("type", Poco::Dynamic::Var(type.c_str()));

}
