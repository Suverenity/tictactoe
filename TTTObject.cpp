#include "TTTObject.h"
#include <typeinfo>

TTTObject::TTTObject(std::string name)
	:m_name(name)
{
	if(name.empty()){
		m_name = typeid(this).name();
	}
}

std::string TTTObject::getName() const
{
	return this->m_name;
}
