#ifndef MESSAGEFACTORY_H
#define MESSAGEFACTORY_H

#include <string>
#include "Tile.h"

class Message;

/**
 * @brief Basic MessageFactory used to format JSON messages
 */

class MessageFactory
{
public:
	MessageFactory() = default;
	Message createCoordsMessage(std::string name, unsigned int x, unsigned int y);
	Message createEndGameMessage(std::string winner);
	Message createErrorMessage(std::string name, std::string messageStr);
	Message createBoardStatusMessage(const TileArray board);
	Message createGameStatusMessage(bool running);
};

#endif // MESSAGEFACTORY_H
