#ifndef TTTObject_H
#define TTTObject_H

#include "ITTTObject.h"

#include <string>

/**
 * @brief The TTTObject is implementation of ITTTObject
 */

class TTTObject
		: virtual public ITTTObject
{
public:
	TTTObject(std::string name);
	std::string getName() const;
protected:
	std::string m_name;

};

#endif // TTTObject_H
