#include "Game.h"

#include <iostream>

/** \mainpage User's manual
 *
 * \section intro_sec Introduction
 *
 * Welcome to the manual to the game TicTacToe. This game is classical game of tic tac toe with a twist,
 * game is played on the N x N board with web browser UI and basic AI. Software is distributed as source code from
 * git repository: git@bitbucket.org:Suverenity/tictactoe.git .
 *
 * \section install_sec Installation
 *
 * Because the game is distributed as source code you have to have following requirements installed
 * and properly set up:
 *	- linux OS (game was tested on Mint 18.1 and Ubuntu 17.04)
 *	- library Boost 1.64
 *	- library Poco 1.7.8, concretely PocoNet, PocoJSON and PocoFoundation
 *	- GCC with version that is supporting C++11
 *	- Cmake with version 2.8 or higher
 *
 * In order to compile and run game use following: <br>
 * \code{.sh} cmake . && cmake --build . --target TicTacToe && ./TicTacToe \endcode
 *
 * Note: Game might properly work and compile even on Windows platform, however this was not tested.
 *
 * \section Control
 *
 * When the game is run default browser with localhost:9000 should appear. If it does not appear
 * type localhost:9000 to the browser. After the browser is properly showing web UI of the game, one can
 * choose size of the game, winning length and who starts next round. Overlay with a game settings disappears
 * after Start Game! button is clicked and is replaced by square mesh of chosen parameters. Tiles change
 * color by the player whose move was placed on given tile, human uses blue color and computer red color.
 * After the round is ended same settings overlay appears and next round can be played. The game is turned off
 * by closing the browser tab/window with game UI.
 *
 * Note: AI works only in 3x3 and 4x4 games, because of its algorithm
 * (details are mentioned in the Computer class documentation).
 *
 * \section Implementation
 *
 * This section serves as basic explanation how the game works. For more detailed documentation
 * go to the individual classes section. As this game has web UI written in JavaScript, it has to have means
 * of communication with browser, which is done via WebSocket protocol. In order to make game responsive
 * the implementation is composed from two parts, and each of them has its own thread.
 *
 * The first part is composed only from game algorithm and classes. Owner of all of these parts is named
 * the Engine class, which is responsible for running every round of the game. This class also holds both
 * players presented. When a round is started this class blocks executing thread until round is finished.
 * The Engine basicaly asks in a loop both players for next move and than this move is send to the Board where it is
 * evaluated if it is possible or impossible. Details about interaction with Player by its consumer are mentioned
 * in its documentation.
 *
 * The second part is targeted to the communication with web browser and with game part of the implementation.
 * Basis of this part is the Communicator class that handles all communication with web browser via
 * WebSocket protocol that has implementation in both Poco and Javascript. Details are mentioned in the
 * Communicator class documentation.
 *
 *
 */


int main(int argc, char *argv[])
{
	std::cout<<"starting game" <<std::endl;
	Game g;
	g.initialize();
	g.run();
	std::cout<<"ending game" <<std::endl;
	return 0;
}
