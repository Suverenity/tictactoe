/**
 * Boost library does raise warnings (listed below) during compilation,
 * so this file is used to disable them to prevent confusion.
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
