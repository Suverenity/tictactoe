#ifndef JSonSender_H
#define JSonSender_H

#include "ConnectionTypes.h"
#include "MessageFactory.h"

/**
 * @brief The MessageSender class
 *
 * Base class for every object that needs to communicate,
 * also contains MessageFactory class to format messages.
 */

class MessageSender{
public:
	MessageSender(MessageFactory& fact);

	/**
	 * @brief Connect internal signal to given slot
	 * @param slot
	 */
	void connectSlotToSignal(MessageSlot slot);
protected:
	void sendMessage(Message msg);
	MessageFactory &m_fact;
private:
	MessageSignal m_sendMessageSignal;
};

#endif // JSonSender_H
