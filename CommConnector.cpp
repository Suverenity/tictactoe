#include "CommConnector.h"
#include "Communicator.h"
#include "MessageSender.h"
#include "MessageReciever.h"

CommConnector::CommConnector(Communicator &comm)
	: m_comm(comm)
{ }

void CommConnector::makeConnections(std::vector<std::shared_ptr<MessageReciever> > recievers,
									std::vector<std::shared_ptr<MessageSender> > senders)
{
	for(auto sender: senders){
		sender->connectSlotToSignal(m_comm.convertSendMsgToBoostFnc());
	}
	for(auto reciever: recievers){
		auto sig = std::make_shared<MessageSignal>();
		reciever->connectToSignal(sig);
		m_comm.m_commMaps.emplace(reciever->getName(), sig);
	}
}

void CommConnector::makeConnections(std::vector<std::reference_wrapper<MessageReciever> > recievers,
									std::vector<std::reference_wrapper<MessageSender> > senders)
{
	for(auto sender: senders){
		sender.get().connectSlotToSignal(m_comm.convertSendMsgToBoostFnc());
	}
	for(auto reciever: recievers){
		auto sig = std::make_shared<MessageSignal>();
		reciever.get().connectToSignal(sig);
		m_comm.m_commMaps.emplace(reciever.get().getName(), sig);
	}
}
