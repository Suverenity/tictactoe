#ifndef HUMAN_H
#define HUMAN_H

#include "Player.h"
#include "MessageReciever.h"

/**
 * @brief The Human player class
 *
 * Apart from Computer class this is simple class that just processes
 * message from UI with next human move.
 */

class Human
		: public Player
		, public MessageReciever
{
public:
	Human(MessageFactory &fact);
	virtual ~Human() = default;

	// Player interface
public:
	/**
	 * @brief Implementation of Player's function.
	 */
	virtual void figureNextMove(Board) override;

	// MessageReciever interface
private:
	void recieveMessage(Message msg);
};

#endif // HUMAN_H
