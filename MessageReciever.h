#ifndef JSONRECIEVER_H
#define JSONRECIEVER_H

#include "ConnectionTypes.h"
#include "TTTObject.h"

/**
 * @brief The MessageReciever class
 *
 * The second to pair with MessageSender, this class recieves messages
 * from connected signal.
 */

class MessageReciever : virtual public ITTTObject
{
public:
	virtual ~MessageReciever() = default;

	/**
	 * @brief Connect to chosen signal (e.g. Communicator)
	 * @param signal
	 */
	void connectToSignal(std::shared_ptr<MessageSignal> signal){
		signal->connect(boost::bind(&MessageReciever::recieveMessage, this, _1));
	}
protected:

	/**
	 * @brief Slot where messages from connected signal are sent.
	 * @param Message
	 */
	virtual void recieveMessage(Message msg) = 0;
};

#endif // JSONRECIEVER_H
