#ifndef TILE_H
#define TILE_H

#include "disable_warnings.h"
#include <boost/multi_array.hpp>
#include "enable_warnings.h"

/**
 * @brief Structure used as coordinations representation
 */

struct Coords{
	Coords(size_t x, size_t y) : x(x), y(y){}
	Coords() :x(0), y(0){}

	std::string toString(){
		return {"[x: " + std::to_string( x ) + ",y: " + std::to_string(y) + "]"};
	}

	friend std::ostream& operator<< (std::ostream& os, const Coords& coords){
		os << "[x: " << coords.x << ",y: " << coords.y << "]";
		return os;
	}

	size_t x = 0;
	size_t y = 0;
};

/**
 * @brief Signalises if tile of the board was played and by whom
 */

enum class Tile{
	noOne,
	human,
	computer
};

/**
 * @brief Result of the board
 *
 * If one wish to get actual winner that information is obtainable from
 * the board class by the getWinner() method.
 */

enum class Result{
	undecided,
	draw,
	victory
};

using TileArray = boost::multi_array<Tile, 2>;


#endif // TILE_H
