#ifndef ENGINE_H
#define ENGINE_H

#include "MessageSender.h"
#include "Board.h"

#include <vector>
#include <memory>

class Player;

/**
 * @brief The game engine class
 *
 * This class serves as base to all game stuff. Serves as owner of the Board
 * class and players classes and as interface for one TicTacToe round.
 */

class Engine
		: public MessageSender
{
public:
	using PlayerBase = std::vector<std::shared_ptr<Player>>;
	Engine(MessageFactory &fact);

	/**
	 * @brief Starts one round.
	 * @param startingPlayer
	 * @param boardSize
	 * @param winningLength
	 */
	void startRound(std::string startingPlayer, size_t boardSize, size_t winningLength);

	/**
	 * @brief Ends current round.
	 *
	 * Returns Engine class to the state where it is possible to start next round.
	 */
	void endRound();

	/**
	 * @brief Players setters.
	 * @param players
	 *
	 * Sets players for given round. It is usually not necessary to setup every round.
	 */
	void setPlayers(PlayerBase players);

private:
	void nextTurn();
	PlayerBase::iterator m_activePlayer;
	PlayerBase m_playerBase;
	Board m_board;
	bool m_endRequested = false;

	void confirmEndAndClean();
	void setStartingPlater(std::string playerName);
	void run();
};

#endif // ENGINE_H
