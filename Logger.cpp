#include "Logger.h"

#include <iostream>

#include "disable_warnings.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include "enable_warnings.h"

void Logger::log(std::string message)
{
	std::cout << boost::posix_time::microsec_clock::local_time().time_of_day() << ":"
			  << "th.id: " << boost::this_thread::get_id() << ": ";
	std::cout << message << std::endl;
}
