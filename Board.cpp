#include "Board.h"
#include "Logger.h"
#include "Message.h"


Board::Board(size_t size)
	: TTTObject("board")
{
	setEdgeLength(size);
}

Board::Board()
	: TTTObject("board")
{
	setEdgeLength(3);
}

Board::Board(Board &other)
	: TTTObject(other.m_name)
	, m_size(other.m_size)
	, m_freeTiles(other.m_freeTiles)
	, m_boardState(other.m_boardState)
	, m_board(other.m_board)
	, m_winLength(other.m_winLength)
	, m_winner(other.m_winner)
{}

size_t Board::getEdgeLength() const
{
	return m_size;
}

size_t Board::getWinLength() const
{
	return m_winLength;
}

void Board::setEdgeLength(size_t value)
{
	m_size = value;
	resetBoard();
}

void Board::setWinLength(size_t value)
{
	m_winLength = value;
}

bool Board::fillTile(size_t row, size_t column, Tile value)
{
	if (row > m_size || row < 0
			|| column > m_size || column < 0
			|| m_board[row][column] != Tile::noOne){
		return false;
	}
	m_board[row][column] = value;
	m_freeTiles --;
	if(!checkWin(row,column, value) && checkDraw()){}
	return true;
}

Result Board::getBoardState()
{
	return m_boardState;
}

Tile &Board::operator()(size_t row, size_t column)
{
	if (row >= m_size || column >= m_size ){
		throw std::out_of_range("board indeces out of range");
	}
	return m_board[row][column];
}

Tile Board::operator()(size_t row, size_t column) const
{
	if (row >= m_size || column >= m_size ){
		throw std::out_of_range("board indeces out of range");
	}
	return m_board[row][column];
}

Board &Board::operator =(Board other)
{
	swap(*this, other);
	return *this;
}

void swap(Board &first, Board &second)
{
	std::swap(first.m_size, second.m_size);
	std::swap(first.m_freeTiles, second.m_freeTiles);
	std::swap(first.m_boardState, second.m_boardState);
	std::swap(first.m_board, second.m_board);
	std::swap(first.m_winLength, second.m_winLength);
	std::swap(first.m_winner, second.m_winner);
}

std::ostream &operator<<(std::ostream &os, const Board &board)
{
	os<< "    ";
	for(unsigned i = 0 ; i < board.m_size; i++){
		os<< i;
		if(i < 10){
			os << " ";
		}
	}
	os<<std::endl;
	for(unsigned i = 0 ; i < board.m_size; i++){
		os<< board.m_board.size() - i -1 << ": ";
		if(board.m_board.size() -i -1 < 10){
			os << " ";
		}
		for(unsigned j = 0 ; j < board.m_size; j++){
			if(board.m_board[i][j] == Tile::noOne){
				os << ". ";
				continue;
			}
			os << static_cast<int>(
				board.m_board[i][j]) << " ";
		}
		os << std::endl;
	}
	return os;
}

void Board::resetBoard()
{
	m_boardState = Result::undecided;
	TileArray::extent_gen extents;
	m_freeTiles = m_size * m_size;
	m_board.resize(extents[m_size][m_size]);
	for(unsigned i = 0; i < m_size ; ++i){
		for(unsigned j = 0; j < m_size ; ++j){
			m_board[i][j] = Tile::noOne;
		}
	}
}

std::string Board::getWinner() const
{
	return m_winner;
}

bool Board::isTileEmpty(Coords coords)
{
	return m_board[coords.y][coords.x] == Tile::noOne;
}

bool Board::isEmpty()
{
	return  m_freeTiles == m_size*m_size;
}

bool Board::checkDraw()
{
	if( m_freeTiles ){
		return false;
	}
	m_boardState = Result::draw;
//	std::cout << "draw" <<std::endl;
//	std::cout << *this << std::endl;
	return true;
}

bool Board::checkWin(size_t row, size_t column, Tile value)
{
	if(checkWinInRow(1,1,row, column, value)
			|| checkWinInRow(1,-1, row, column, value)
			|| checkWinInRow(1,0,row, column, value)
			|| checkWinInRow(0,1,row, column, value)){
		m_boardState = Result::victory;
//		std::cout << "win" << std::endl;
//		std::cout << *this << std::endl;
		switch (value) {
		case Tile::computer:
			m_winner = "computer";
			break;
		case Tile::human:
			m_winner = "human";
		default:
			break;
		}
		return true;
	}
	return false;
}

bool Board::checkWinInRow(int xDir, int yDir, size_t rootRow,
						  size_t rootColumn, Tile value)
{
	Coords tile, oppositeTile;
	bool failed = false, oppositeFailed = false;
	WinRecord winRecord;
	winRecord.push_back({rootColumn, rootRow});
	for(size_t i = 1; i < m_winLength; i++){
		tile.x = rootColumn + xDir*i;
		tile.y = rootRow + yDir*i;
		oppositeTile.x = rootColumn + xDir*(-i);
		oppositeTile.y = rootRow + yDir*(-i);
		if(checkWinInHalf({rootColumn, rootRow}, tile, value,
					   failed, winRecord, false)
		   || checkWinInHalf({rootColumn, rootRow}, oppositeTile, value,
					   oppositeFailed, winRecord, true)){
//			std::cout <<"[" << winRecord.front().x << ", " << winRecord.front().y
//					  << "; " <<winRecord.back().x << ", " << winRecord.back().y <<"]"
//					  << std::endl;
			return true;
		}
		if(failed && oppositeFailed){
			break;
		}
	}
	return false;
}

bool Board::checkWinInHalf(Coords root, Coords other, Tile value, bool& failedAdding,
						   WinRecord& winRecord, bool oppositeDir)
{
	if(!failedAdding && checkTwoTilesToValue(root.y, root.x,
						 other.y, other.x, value)){
		if(oppositeDir){
			winRecord.emplace_back(other);
		}
		else {
			winRecord.emplace_front(other);
		}
		if(winRecord.size() == m_winLength){
			return true;
		}
	} else {
		failedAdding = true;
	}
	return false;
}

bool Board::checkTwoTilesToValue(size_t rowFirst, size_t columnFirst,
								 size_t rowSecond, size_t columnSecond,
								 Tile value)
{
	try{
		if(((*this)(rowFirst, columnFirst) == (*this)(rowSecond, columnSecond))
			  && (value == (*this)(rowSecond, columnSecond))){
			return true;
		}
		return false;
	} catch (std::out_of_range){
		return false;
	}
}
