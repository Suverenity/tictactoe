#include "Message.h"
#include "Logger.h"
#include "Human.h"
#include "Board.h"

#include <random>
#include <string>

Human::Human(MessageFactory &fact)
	:Player("human", Tile::human, fact)
{ }

void Human::figureNextMove(Board)
{
	m_hasNextMove = false;
}

void Human::recieveMessage(Message msg)
{
	if(msg.getName() != this->getName()){
		Logger::log("wrong message in human");
		return ;
	}
	auto data = msg.getData();
	m_nextMove.x = data->get("x").convert<int>();
	m_nextMove.y = data->get("y").convert<int>();
	m_hasNextMove = true;
}
