#ifndef COMMCONNECTOR_H
#define COMMCONNECTOR_H

#include <vector>
#include <memory>

class Communicator;
class MessageSender;
class MessageReciever;

/**
 * @brief The CommConnector class
 * Meant only as creating connection manager
 */

class CommConnector
{
public:
	CommConnector(Communicator &comm);

	void makeConnections(std::vector<std::shared_ptr<MessageReciever>> recievers,
						 std::vector<std::shared_ptr<MessageSender>> senders);

	void makeConnections(std::vector<std::reference_wrapper<MessageReciever>> recievers,
						 std::vector<std::reference_wrapper<MessageSender>> senders);

private:
	Communicator &m_comm;
};

#endif // COMMCONNECTOR_H
