#include "MessageFactory.h"
#include "Message.h"

Message MessageFactory::createCoordsMessage(std::string name, unsigned int x, unsigned int y)
{
	Message msg(name, "coords");
	Json::Object data;
	data.set("x",x);
	data.set("y", y);
	msg.setData(data);
	return msg;
}

Message MessageFactory::createEndGameMessage(std::string winner)
{
	Message msg("game", "end");
	Json::Object data;
	data.set("winner", winner);
	msg.setData(data);
	return msg;
}

Message MessageFactory::createErrorMessage(std::string name, std::string messageStr)
{
	Message msg(name, "error");
	Json::Object data;
	data.set("msg", messageStr);
	msg.setData(data);
	return msg;
}

Message MessageFactory::createBoardStatusMessage(const TileArray board)
{
	Message msg("board", "status");
	Json::Object data;

	Json::Array jsonBoard;
	for(unsigned i=0 ; i < board.shape()[0]; ++i){
		Json::Array jsonBoardColumn;
		for(unsigned j=0 ; j < board.shape()[1]; ++j){
			Json::Object tile;
			std::string played;
			switch (board[i][j]) {
			case Tile::computer:
				played = "computer";
				break;
			case Tile::human:
				played = "human";
				break;
			case Tile::noOne:
				played = "unplayed";
				break;
			}
			tile.set("played", played);
			jsonBoardColumn.add(tile);
		}
		jsonBoard.add(jsonBoardColumn);
	}
	data.set("board", jsonBoard);
	msg.setData(data);
	return msg;
}

Message MessageFactory::createGameStatusMessage(bool running)
{
	Message msg("game", "status");
	Json::Object data;
	data.set("running", running);
	msg.setData(data);
	return msg;
}
