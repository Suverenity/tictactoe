#ifndef COMPUTER_H
#define COMPUTER_H

#include "Player.h"

#include <vector>
#include <functional>

/**
 * @brief The Computer class
 *
 * Computer AI is based on minmax [1] algorithm.
 *
 * Naive search of minmax was improved by remembering depth of last succesfull
 * round ignoring branches in tree of states that go deeper.
 * However, this algorithm works properly only on NxN settings with winning length of N.
 * To achieve bearable results one should play only 3x3 or 4x4 where is algorithm enabled.
 *
 * todo: Implement alpha-beta pruning algorithm [2] based new class of game state that is defined
 * as an array of sums of player moves where one player is 1 and other is -1.
 *
 * [1] http://neverstopbuilding.com/minimax
 * [2] https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning
 *
 */

class Computer
		: public Player
{
	enum class MinMaxRes{
		HumanWon = -1,
		ComputerWon = 1,
		Draw = 0,
	};
public:
	using MinMaxRecord = std::pair<MinMaxRes, Coords>;
	using MinMaxRecordVector = std::vector<MinMaxRecord>;

	Computer(MessageFactory& fact);
	virtual ~Computer () = default;
	std::function<bool(const MinMaxRecord&, const MinMaxRecord&)> cmpFunction =
		[](const MinMaxRecord& a, const MinMaxRecord& b) -> bool{
			return a.first < b.first;
		};

	// Player interface
public:
	virtual void figureNextMove(Board board) override;

private:
	MinMaxRes minmax(Board board, Tile val, unsigned long &succesfullDepth, unsigned long depth, Coords coords);
	Coords minmaxStart(Board board);
	bool m_played = false;
};

#endif // COMPUTER_H
