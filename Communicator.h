#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include "ConnectionTypes.h"

#include <memory>
#include <fstream>
#include <functional>
#include <vector>
#include <map>

#include "disable_warnings.h"
#include <boost/thread/pthread/mutex.hpp>
#include <boost/signals2/signal.hpp>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include "enable_warnings.h"

#define PORT 9000

namespace Poco{
namespace Net{
	class WebSocket;
}
}

namespace boost{
class thread;
}

class Message;
class MessageFactory;
class CommConnector;

using WSFacFunc = std::function<void(
	Poco::Net::HTTPServerRequest &request,
	Poco::Net::HTTPServerResponse &response)>;

/**
 * @brief The Communicator class
 *
 * Communicator runs in its own thread and manages every communication from
 * web UI and uses signals from Boost library to communicate with the rest
 * of the application and it also uses Poco::Net::HTTPServer
 * and its HTTPRequestHandlerFactory  (CommFactory class) to handle
 * HTTP request frames. In order to communicate with the rest of the application,
 * signals from Boost library are used. Connection of the Communicator class
 * with the rest of the application is done in the CommConnector class.
 *
 * Communication with the browser is handled by WebSocket protocol that has
 * implementation in Poco library as Poco::Net::WebSocket. However, as this object
 * is created one must poll messages from object in regular basis. In order to
 * poll regularly and in order to application remain responsive it is necessary
 * to use different thread for polling.
 */

class Communicator
{
	friend class CommConnector;
public:

	Communicator(MessageFactory &fact);
	~Communicator();
	void start();
	void stop();

private:
	void sendMessage(Message msg);
	void threadRun();
	void createWebsocket(Poco::Net::HTTPServerRequest &request,
						 Poco::Net::HTTPServerResponse &response);
	void checkWsMessages();
	void sendMessages();

	boost::function<void (Message)> convertSendMsgToBoostFnc();

	Poco::Net::HTTPServer server;
	std::shared_ptr<boost::thread> worker;
	std::shared_ptr<Poco::Net::WebSocket> m_socket;
	bool m_keepRunning = false;
	std::vector<Message> m_messageBuffer;
	boost::mutex m_mutex;
	std::map<std::string, std::shared_ptr<MessageSignal>> m_commMaps;
	bool m_browserOpened = false;
	MessageFactory &m_fact;
};

/**
 * @brief Implementation of Poco::Net::HTTPRequestHandlerFactory
 *
 * Creates handlers depending on HTTP request. There are used ExternalFileLoaders
 * for Js, CSS files and index.html, and Websocket request handler, that creates WebSocket
 * object in Communicator. Is used by Poco::Net::HTTPServer in Communicator class.
 */

class CommFactory : public Poco::Net::HTTPRequestHandlerFactory
{
	// HTTPRequestHandlerFactory interface
public:
	CommFactory() = default;
	CommFactory(WSFacFunc facFunc);
	virtual Poco::Net::HTTPRequestHandler *createRequestHandler(
			const Poco::Net::HTTPServerRequest &request) override;
private:
	WSFacFunc m_facFunc;
};


class WsRequest : public Poco::Net::HTTPRequestHandler
{
	// HTTPRequestHandler interface
public:
	WsRequest(WSFacFunc facFunc);
	virtual void handleRequest(
			Poco::Net::HTTPServerRequest &request,
			Poco::Net::HTTPServerResponse &response) override;
private:
	WSFacFunc m_facFunc;
};

class ExternalFileLoader : public Poco::Net::HTTPRequestHandler
{
public:
	virtual ~ExternalFileLoader() = default;

protected:
	void loadFile(std::ostream& ostr, std::string filename);
};

class PageRequest : public ExternalFileLoader
{
	// HTTPRequestHandler interface
public:
	virtual void handleRequest(
			Poco::Net::HTTPServerRequest &,
			Poco::Net::HTTPServerResponse &response) override;
};

class JavaScriptRequest: public ExternalFileLoader
{
	// HTTPRequestHandler interface
public:
	virtual void handleRequest(
			Poco::Net::HTTPServerRequest &request,
			Poco::Net::HTTPServerResponse &response) override;
};

class CSSRequest: public ExternalFileLoader
{
	// HTTPRequestHandler interface
public:
	void handleRequest(
			Poco::Net::HTTPServerRequest &request,
			Poco::Net::HTTPServerResponse &response);
};


#endif // COMMUNICATOR_H
