#include "Engine.h"
#include "Computer.h"
#include "Human.h"
#include "Logger.h"

#include <iostream>
#include <algorithm>
#include <random>
#include <iostream>

#include "disable_warnings.h"
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "enable_warnings.h"


Engine::Engine(MessageFactory &fact)
	: MessageSender(fact)
{}

void Engine::run()
{
	while(!m_endRequested && (m_board.getBoardState() == Result::undecided)){
		(*m_activePlayer)->figureNextMove(m_board);
		while( (!(*m_activePlayer)->hasNextMove()) &&  !m_endRequested ){
			boost::this_thread::sleep(boost::posix_time::milliseconds(100));
		}
		auto coords = (*m_activePlayer)->getNextMove();
		if(m_board.fillTile(coords.y, coords.x, (*m_activePlayer)->getTileType())){
			(*m_activePlayer)->moveAccepted();
			this->nextTurn();
		} else {
			Logger::log("trying to play: " + coords.toString());
			std::cout << m_board << std::endl;
			continue;
		}
	}
	confirmEndAndClean();
}

void Engine::startRound(std::string startingPlayer, size_t boardSize, size_t winningLength)
{
	if(startingPlayer.empty()
			|| boardSize < 3 || boardSize > 101
			|| boardSize < 3 || boardSize > 101){
		std::string errMsg("Wrong parameters to start game: starting player: "
						   + startingPlayer
						   + ", board size: " + std::to_string(boardSize)
						   + ", winning length: " + std::to_string(winningLength));
		throw (std::invalid_argument(errMsg));
	}
	m_board.setEdgeLength(boardSize);
	m_board.setWinLength(winningLength);
	try{
		this->setStartingPlater(startingPlayer);
	} catch( std::out_of_range& ex){
		this->sendMessage(m_fact.createErrorMessage("game", ex.what()));
		return;
	}
	run();
}

void Engine::endRound()
{
	for(auto player : m_playerBase){
		player->endRound();
	}
	m_endRequested = true;
}

void Engine::setPlayers(Engine::PlayerBase players)
{
	m_playerBase = players;
	m_activePlayer = m_playerBase.begin();
}

void Engine::nextTurn()
{
	if(++m_activePlayer == m_playerBase.end()){
		m_activePlayer = m_playerBase.begin();
	}
}

void Engine::confirmEndAndClean()
{
	if(!m_endRequested){
		std::string winner;
		switch (m_board.getBoardState()) {
		case Result::undecided:
		{
			std::string ex("game is undecided when should be over");
			sendMessage(m_fact.createErrorMessage("game", ex));
			throw std::string (ex);
			break;
		}
		case Result::victory:
			winner = m_board.getWinner();
			break;
		case Result::draw:
			winner = "draw";
			break;
		default:
			Logger::log("unmapped board state in endGame");
			break;
		}
		if(!winner.empty()){
			sendMessage(m_fact.createEndGameMessage(winner));
		}
	}
	m_board.resetBoard();
	m_endRequested = false;
}

void Engine::setStartingPlater(std::string playerName)
{
	auto activePlayerPos = std::find_if(
				std::begin(m_playerBase),
				std::end(m_playerBase),
				[playerName](std::shared_ptr<Player> player){
					return player->getName() == playerName;
				});
	if(activePlayerPos != m_playerBase.end()){
		m_activePlayer = activePlayerPos;
	} else {
		throw std::out_of_range("starting player not found");
	}

}
