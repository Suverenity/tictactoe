#include "Communicator.h"
#include "Logger.h"
#include "Message.h"
#include "MessageFactory.h"

#include "disable_warnings.h"
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/WebSocket.h>
#include <Poco/Net/NetException.h>
#include <Poco/Net/ServerSocket.h>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include "enable_warnings.h"

#include <iostream>


namespace fs = boost::filesystem;

Communicator::Communicator(MessageFactory& fact)
	: server(new CommFactory(
				 [this](
					Poco::Net::HTTPServerRequest &request,
					Poco::Net::HTTPServerResponse &response){
						this->createWebsocket(request,response);
			 }),
			 Poco::Net::ServerSocket(PORT),
			 new Poco::Net::HTTPServerParams),
			 m_fact(fact)
{ }

Communicator::~Communicator()
{
	this->stop();
}

void Communicator::start()
{
	this->worker = std::make_shared<boost::thread>(
				boost::bind(&Communicator::threadRun, this));
}

void Communicator::stop()
{
	if(m_keepRunning){
		m_keepRunning = false;
		if(this->worker->joinable()){
			this->worker->join();
		}
		Logger::log("joined");
	}
}

void Communicator::sendMessage(Message msg)
{
	this->m_messageBuffer.push_back(msg);
}

void Communicator::threadRun()
{
	server.start();
	m_keepRunning = true;
	while (m_keepRunning) {
		if(m_socket){
			this->checkWsMessages();
			this->sendMessages();
		}
		boost::this_thread::sleep(boost::posix_time::milliseconds(100));
	}
	server.stop();
}

void Communicator::createWebsocket(
		Poco::Net::HTTPServerRequest &request,
		Poco::Net::HTTPServerResponse &response)
{
	if(!this->m_socket){
		m_socket = std::make_shared<Poco::Net::WebSocket>(request,response);
	}
}

void Communicator::checkWsMessages()
{
	if(m_socket){
		int flags, res;
		int availableBytes = m_socket->available();
		if(availableBytes < 1){
			return;
		}
		std::string buffer(availableBytes, ' ');
		res = m_socket->receiveFrame(&buffer[0], buffer.length(),flags);
		if(res <= 0 ||
				(flags & Poco::Net::WebSocket::FRAME_OP_BITMASK)
				== Poco::Net::WebSocket::FRAME_OP_CLOSE){
			return;
		}
		Logger::log("recieved message: " + buffer);
		Message msg(buffer);
		auto signal = m_commMaps.at(msg.getName());
		(*signal.get())(msg);
	}
}

void Communicator::sendMessages()
{
	if(!m_socket){
		Logger::log("socket not ready when it should be");
		return;
	}
	m_mutex.lock();
	for(auto &message: m_messageBuffer){
		std::string msgToSend = message.toString();
		Logger::log("sending: " + msgToSend);
		if(m_socket->sendBytes(msgToSend.c_str(), msgToSend.length(),
							   Poco::Net::WebSocket::FRAME_FLAG_FIN
							   + Poco::Net::WebSocket::FRAME_OP_TEXT) <= 0){
			Logger::log("msg sending error: " + message.toString());
		}
	}
	m_messageBuffer.clear();
	m_mutex.unlock();
}

boost::function<void (Message)> Communicator::convertSendMsgToBoostFnc()
{
	return boost::bind(&Communicator::sendMessage, this, _1);
}

/////CommFactory/////

CommFactory::CommFactory(WSFacFunc facFunc)
	:m_facFunc(facFunc)
{ }

Poco::Net::HTTPRequestHandler *CommFactory::createRequestHandler(
		const Poco::Net::HTTPServerRequest &request)
{
	auto uri = request.getURI();
	Logger::log("request uri: " +  uri);

	if(request.find("Upgrade") != request.end()
		&& Poco::icompare(request["Upgrade"], "websocket") == 0){
			Logger::log("WsRequest");
			return new WsRequest(m_facFunc);
	}
	if(uri.find(".css") != std::string::npos){
		Logger::log("CssRequest" );
		return new CSSRequest;
	}
	if(uri.find(".js") != std::string::npos){
		Logger::log("JavaScriptRequest" );
		return new JavaScriptRequest;
	}
	if(uri=="/"){
		Logger::log("PageRequest" );
		return new PageRequest;
	}

	return nullptr;
}

/////WsRequest/////

WsRequest::WsRequest(WSFacFunc facFunc)
	:m_facFunc(facFunc)
{ }

void WsRequest::handleRequest(
		Poco::Net::HTTPServerRequest &request,
		Poco::Net::HTTPServerResponse &response)
{
	try{
		m_facFunc(request,response);
	} catch (Poco::Net::WebSocketException & ex){
		Logger::log( "problem with websocket" );
		Logger::log(ex.message());
	}
}

////PageRequest/////

void PageRequest::handleRequest(
		Poco::Net::HTTPServerRequest &,
		Poco::Net::HTTPServerResponse &response)
{
	response.setContentType("text/html");
	response.setChunkedTransferEncoding(true);
	std::ostream& ostr = response.send();
	this->loadFile(ostr, "index.html");
}

//JSRequest

void JavaScriptRequest::handleRequest(
		Poco::Net::HTTPServerRequest &request,
		Poco::Net::HTTPServerResponse &response)
{
	response.setContentType("text/html");
	response.setChunkedTransferEncoding(true);
	std::ostream& ostr = response.send();
	auto uri = request.getURI();
	this->loadFile(ostr, uri.substr(
					   uri.find_last_of("/")+1));
}

//CSSRequest

void CSSRequest::handleRequest(
		Poco::Net::HTTPServerRequest &request,
		Poco::Net::HTTPServerResponse &response)
{
	response.setContentType("text/css");
	response.setChunkedTransferEncoding(true);
	std::ostream& ostr = response.send();
	auto uri = request.getURI();
	this->loadFile(ostr, uri.substr(
					   uri.find_last_of("/")+1));
}

void ExternalFileLoader::loadFile(std::ostream &ostr, std::string filename)
{
	fs::path filePath = fs::current_path() / fs::path("javascript")	/ fs::path(filename);
	std::ifstream ifstr ( filePath.c_str() );
	if(!ifstr.good()){
		Logger::log(std::string("problem with file: ") + std::string(filePath.c_str()));
		return;
	}
	while(ifstr.good()){
		auto intcode = ifstr.get();
		if(intcode < 0){
			continue;
		}
		ostr << static_cast<char> (intcode);
	}
	ifstr.close();
	ostr.flush();
}
