#ifndef MESSAGE_H
#define MESSAGE_H

#include "enable_warnings.h"
	#include <Poco/JSON/Object.h>
#include "disable_warnings.h"

namespace Json = Poco::JSON;

/**
 * @brief The Message class
 *
 * Wrap of Poco JSON message meant to easily format message send to web UI
 * by WebSocket.
 *
 * All types and formats of JSON messages are listed in "documentation/JSON_messages"
 */

class Message
{
public:
	Message(std::string name, std::string msgType);
	Message(std::string msgJson);
	void setName(std::string name);
	void setData(Json::Object data);
	std::string getName();
	std::string toString();
	Json::Object::Ptr getData();
	std::string getType();
	void setType(std::string);

private:
	Json::Object::Ptr m_data;

};

#endif // MESSAGE_H
