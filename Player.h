#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "TTTObject.h"
#include "MessageSender.h"
#include "Tile.h"


class Board;

/**
 * @brief Base of Human and Computer player classes
 *
 * Since game has to wait for response from the UI (aka from thread running
 * inside Communicator class) in case of Human player, it is necessary to create
 * mechanism that allows game itself to ask Players whether next move is ready.
 *
 * This mechanism is made by figureNextMove abstract method that ask Player class
 * to figure out next move. In case of human it means that waiting for human response
 * from web is expected, and in case of computer it starts AI algorithm.
 *
 * Method hasNextMove() is used to signal that player made his decision about next move,
 * which can be obtained by getNextMove().
 *
 * At last, method moveAccepted() should be called when move was accepted.
 */

class Player
		: public TTTObject
		, public MessageSender
{
public:
	Player(std::string name, Tile tile, MessageFactory &fact);
	virtual ~Player() = default;

	/**
	 * @brief Method used for starting player's decision making about next move.
	 * @param board
	 */
	virtual void figureNextMove(Board board) = 0;

	/**
	 * @brief Getter for next move.
	 * @return coordinates of next move
	 *
	 * One needs to ask player if it has next move prepared with hasNextMove method.
	 */
	Coords getNextMove(){ return m_nextMove; }

	/**
	 * @brief Method for confirming readiness of player's next move.
	 * @return  coordinates of next move
	 *
	 * This function has to be called before asking for next move with getNextMove method.
	 */
	bool hasNextMove(){ return m_hasNextMove; }
	Tile getTileType() { return m_tileType; }

	/**
	 * @brief Should be called after figured move was accepted.
	 *
	 * For example sends message with accepted coordinates to UI.
	 */
	virtual void moveAccepted();

	/**
	 * @brief Ends current thinking about next move.
	 */
	void endRound();

protected:

	Tile m_tileType = Tile::noOne;
	bool m_hasNextMove = false;
	Coords m_nextMove = {0,0};
	bool m_endRequested = false;

};

#endif // PLAYER_H
