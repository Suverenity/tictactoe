#include "Game.h"
#include <boost/thread.hpp>
#include "Human.h"
#include "Computer.h"
#include "Logger.h"
#include "MessageReciever.h"
#include "MessageSender.h"
#include "CommConnector.h"

Game::Game()
	: TTTObject("game"), MessageSender(m_messageFactory)
	, m_comm(m_fact)
	, m_engine(m_messageFactory)
{}

Game::~Game()
{
	m_comm.stop();
}

void Game::startRound()
{
	Logger::log("starting new round");
	if(m_nextRoundSettings.startingPlayer.empty()){
		Logger::log("no starting arguments");
		return;
	}
	m_engine.startRound(m_nextRoundSettings.startingPlayer,
				   m_nextRoundSettings.boardSize,
				   m_nextRoundSettings.winningLength);
	m_nextRoundSettings.reset();
}

void Game::run()
{
	m_comm.start();
	openBrowser();
	while(m_keepRunning){
		try{
			if(!m_nextRoundSettings.startingPlayer.empty()){
				m_roundInProgress = true;
				this->startRound();
			}
			boost::this_thread::sleep(boost::posix_time::milliseconds(500));
		}catch (std::string ex){
			logException(ex + ". Restarting.");
		}catch (std::invalid_argument ex){
			logException(std::string(ex.what()) + ". Restarting.");
		}catch (std::out_of_range ex){
			logException(std::string(ex.what()) + ". Restarting.");
		}catch (...){
			logException("Unknown exception. Restarting game.");
		}
		m_roundInProgress = false;
	}
	m_comm.stop();
}

void Game::initialize()
{
	CommConnector connector(m_comm);
	std::vector<std::reference_wrapper<MessageReciever>> recievers({*this});
	std::vector<std::reference_wrapper<MessageSender>> senders({*this});
	std::vector<std::shared_ptr<Player>> players;
	auto human = std::make_shared<Human>(m_messageFactory);
	auto computer= std::make_shared<Computer>(m_messageFactory);
	players.push_back(human);
	players.push_back(computer);

	senders.emplace_back(*human.get());
	senders.emplace_back(*computer.get());
	senders.emplace_back(m_engine);

	recievers.emplace_back(*human.get());

	connector.makeConnections(recievers, senders);
	m_engine.setPlayers(players);
}

void Game::end()
{
	m_engine.endRound();
	m_keepRunning = false;
}

void Game::openBrowser()
{
	try{
		system("sensible-browser http://localhost:9000 &");
	} catch(...){ Logger::log("browser exception"); }
}

void Game::logException(std::string exMsg)
{
	Logger::log(exMsg);
	this->sendMessage(m_fact.createErrorMessage("game", exMsg));
}

void Game::recieveMessage(Message msg)
{
	if(msg.getName() != this->getName()){
		Logger::log("got wrong msg in Game");
		Logger::log(msg.toString());
		return;
	}

	if(msg.getType() == "start"){
		try {
			Logger::log("got data to the new round");
			Poco::JSON::Object::Ptr data = msg.getData();
			NextRound rnd(data->get("starting").toString(),
						  data->get("size").convert<int>(),
						  data->get("winlength").convert<int>());
			this->m_nextRoundSettings = rnd;
			Logger::log("new round data set");
		} catch (Poco::BadCastException& ex){
			Logger::log(ex.message());
		}
	} else if (msg.getType() == "end"){
		Logger::log("got end game message");
		this->end();
	} else if (msg.getType() == "endRound"){
		Logger::log("got end round message");
		if(m_roundInProgress){
			m_engine.endRound();
		}
	} else {
		Logger::log("unmapped message");
	}
	Logger::log(msg.toString());

}
